package com.calculadora.calculadora2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()

        n_0.setOnClickListener {Acrescentar_expressao(string = "0", limpar_dados = true)}
        n_1.setOnClickListener {Acrescentar_expressao(string = "1", limpar_dados = true)}
        n_2.setOnClickListener {Acrescentar_expressao(string = "2", limpar_dados = true)}
        n_3.setOnClickListener {Acrescentar_expressao(string = "3", limpar_dados = true)}
        n_4.setOnClickListener {Acrescentar_expressao(string = "4", limpar_dados = true)}
        n_5.setOnClickListener {Acrescentar_expressao(string = "5", limpar_dados = true)}
        n_6.setOnClickListener {Acrescentar_expressao(string = "6", limpar_dados = true)}
        n_7.setOnClickListener {Acrescentar_expressao(string = "7", limpar_dados = true)}
        n_8.setOnClickListener {Acrescentar_expressao(string = "8", limpar_dados = true)}
        n_9.setOnClickListener {Acrescentar_expressao(string = "9", limpar_dados = true)}
        n_ponto.setOnClickListener {Acrescentar_expressao(string = ".", limpar_dados = true)}


        // definir click operadores

        mais.setOnClickListener { Acrescentar_expressao("+",true)}
        menos.setOnClickListener { Acrescentar_expressao(string = "-", limpar_dados = true)}
        mult.setOnClickListener { Acrescentar_expressao(string = "*", limpar_dados = true)}
        divisao.setOnClickListener { Acrescentar_expressao(string = "/", limpar_dados = true)}

        limpar.setOnClickListener {
            historico.text = ""
            resultado.text = ""
        }

        back.setOnClickListener{
            val string = historico.text.toString()
            if(string.isNotBlank()){
                historico.text = string.substring(0,string.length-1)
            }
            resultado.text= ""
        }

        igual.setOnClickListener{
            try{
                val expressao = ExpressionBuilder(historico.text.toString()).build()
                val resultado2 = expressao.evaluate()
                val longReusultado = resultado2.toLong()

                if(resultado2 == longReusultado.toDouble())
                    resultado.text = longReusultado.toString()
                else
                    resultado.text = resultado2.toString()

            }catch (e: Exception){

            }
        }



    }

    fun Acrescentar_expressao(string: String, limpar_dados: Boolean){

        if(resultado.text.isNotEmpty()){
        historico.text= ""
        }

        if(limpar_dados){
            resultado.text= ""
            historico.append(string)
            resultado.text = ""
        }
    }



}